import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { TestComponent } from './test/test.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { EmailverifyComponent } from './emailverify/emailverify.component';
import { VerifyotpComponent } from './verifyotp/verifyotp.component';
import { FaqComponent } from './faq/faq.component';


const routes: Routes = [
 
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent  },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'test', component: TestComponent },
  { path: 'ChangePassword', component: ChangepasswordComponent },
  { path: 'emailverify', component: EmailverifyComponent },
  {path:'verifyotp/:userid/:factorid',component:VerifyotpComponent},
  { path: 'faq', component: FaqComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
