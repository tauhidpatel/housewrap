import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  constructor(private http: HttpClient,private translate: TranslateService) {

    translate.setDefaultLang('en');
    environment.language="en";
    this.ngOnInit();
   }
  posts: any[];
  ngOnInit(): void {
    // alert("hiii");
  
 
    this.getPosts().subscribe((data: any[]) => {
    
   //  this.posts = JSON.parse(data.toString()).filter(a => a.status == "ACTIVE" && a.id != "0oa2rnb6sw4FqFlc34x7" && a.id != "0oa2s3zq6qchwJbUH4x7" && a.id != "0oa5gyxfbxiNYSrcq4x6");
   this.posts = JSON.parse(data.toString()).filter(a => a.status == "ACTIVE" && a.id != "0oa1ro27x0HyDsspk0x7"
   && a.id != "0oa3a7ryycBeHfEP80x7"
   && a.id != "0oa3a7rz27i4g4jzI0x7"
   && a.id != "0oa3nx88c3tiNCbyz0x7"
   && a.id != "0oa3uzrmyesTbnWDY0x7"
   && a.id != "0oa4au174w1Rgrsub0x7"
   && a.id != "0oa4dytslgRB1DZZd0x7"
   && a.id != "0oa4eu149wPxhKKDm0x7"
   && a.id != "0oa4ga60w7miKJUH90x7"
   && a.id != "0oa4hidn4pRXQbd6b0x7"
   && a.id != "0oa4yyyw8u3W4kBLc0x7"
   && a.id != "0oa51gz1s6xPyYSLP0x7"
   && a.id != "0oa51rdwizBQUAUub0x7"
   && a.id != "0oafmb6mm4b7rtyGO0x6"
   && a.id != "0oag8czx6veiOKAMX0x6");
    
    // alert(this.posts);
 
     $("#loader").css("display","none")
   }, error=> console.log(error) );
        
   }
   ngAfterViewInit() {
     $("#logout").css("display","block")
   }
   useLanguage(language: string) {
    this.translate.use(language);
  }
   getPosts(): Observable<any[]> {
  
 
 
     let httpParams = new HttpParams();
     
     httpParams = httpParams.append('userid', sessionStorage.getItem("userId")?.toString());
     return this.http.get<any[]>('https://digital-h-1-uat.certainteed.com/OktaAPI/App/GetUserApps', { params: httpParams });  
   }
 

}
