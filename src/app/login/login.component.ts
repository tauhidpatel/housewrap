import { ChangeDetectorRef, Component, NgZone, OnInit } from '@angular/core';
import { Okta } from '../shared/okta/okta.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  title = 'app';
  user;
  oktaSignIn;
  private route: ActivatedRoute;
 

  constructor(private translate: TranslateService,private okta: Okta, private changeDetectorRef: ChangeDetectorRef,private router: Router,private zone: NgZone,private http: HttpClient,) {
    translate.setDefaultLang('en');
   // console.log(environment.language);
   // environment.language="en";
    //console.log(environment.language);
    this.oktaSignIn = okta.getWidget();
    debugger;
  }

  showLogin(): void {
    this.oktaSignIn?.remove();
    this.oktaSignIn.renderEl({el: '#okta-login-container'}, (response) => {
      if (response.status === 'SUCCESS') {
        this.oktaSignIn.authClient.tokenManager.add('idToken', response.tokens.idToken);
        this.oktaSignIn.authClient.tokenManager.add('accessToken', response.tokens.accessToken);
        this.user = response.tokens.idToken.claims.email;
       // alert(this.user);
     //   console.log(response);
        sessionStorage.setItem("userId",response.tokens.idToken.claims.sub);
     //  alert(sessionStorage.getItem("userId")?.toString());
        this.oktaSignIn.remove();
        this.changeDetectorRef.detectChanges();
        this.zone.run(() => {
          this.router.navigate(['/dashboard']); 
      });
       
      }
     
    });
  }

  async ngOnInit(): Promise<void> {
   
  }
  async ngAfterViewInit() {
    $(".arrow2").css("display","none");
    $(".arrow1").css("display","none");
  //  this.showLogin();
    // ngOnInit doesn't work for this :(
    // https://github.com/okta/okta-signin-widget/issues/278
    try {
      // this.oktaSignIn.authClient.session.get(response => {
      //   console.log(response);
      //   if (response.status !== "INACTIVE") {
      //     this.user = response.login;
      //     this.changeDetectorRef.detectChanges();
      //   } else {
      //     console.log("response.status was inactive:", response.status);
      //     this.showLogin();
      //   }
      // });
      var response = await this.oktaSignIn.authClient.session.get();
     // console.log(response);
      if (response.status !== "INACTIVE") {
        sessionStorage.setItem("userId",response.userId);
        this.router.navigate(['/dashboard']); 
      }
      else
      {
        this.showLogin();
      }
    } catch (error) {
    //  console.log(error);
    }
    // show login screen anyway...
   // this.showLogin();
  
   
  }
  logout(): void {
    this.oktaSignIn.signOut(() => {
      this.user = undefined;
      this.showLogin();
    });
  }
}
