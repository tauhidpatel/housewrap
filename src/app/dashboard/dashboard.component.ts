import { Component, OnInit, Pipe } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  Apps: any='';
  posts: any[];
  constructor(private http: HttpClient,private translate: TranslateService,private router: Router) {
    translate.setDefaultLang('en');
   // environment.language="en";
    this.ngOnInit();
  }
  
  
  useLanguage(language: string) {
    this.translate.use(language);
  }

  ngOnInit(): void {
   // alert("hiii");
   if(sessionStorage.getItem("userId")!=null)
   {

   this.getPosts().subscribe((data: any[]) => {
   
  //  this.posts = JSON.parse(data.toString()).filter(a => a.status == "ACTIVE" && a.id != "0oa2rnb6sw4FqFlc34x7" && a.id != "0oa2s3zq6qchwJbUH4x7" && a.id != "0oa5gyxfbxiNYSrcq4x6");
  this.posts = JSON.parse(data.toString()).filter(a => a.status == "ACTIVE" && a.id != "0oa1ro27x0HyDsspk0x7"
  && a.id != "0oa3a7ryycBeHfEP80x7"
  && a.id != "0oa3a7rz27i4g4jzI0x7"
  && a.id != "0oa3nx88c3tiNCbyz0x7"
  && a.id != "0oa3uzrmyesTbnWDY0x7"
  && a.id != "0oa4au174w1Rgrsub0x7"
  && a.id != "0oa4dytslgRB1DZZd0x7"
  && a.id != "0oa4eu149wPxhKKDm0x7"
  && a.id != "0oa4ga60w7miKJUH90x7"
  && a.id != "0oa4hidn4pRXQbd6b0x7"
  && a.id != "0oa4yyyw8u3W4kBLc0x7"
  && a.id != "0oa51gz1s6xPyYSLP0x7"
  && a.id != "0oa51rdwizBQUAUub0x7"
  && a.id != "0oafmb6mm4b7rtyGO0x6"
  && a.id != "0oag8czx6veiOKAMX0x6");
   
   // alert(this.posts);

    $("#loader").css("display","none")
  }, error=> console.log(error) );
}
else
{
  this.router.navigate(['/login']); 
}
  }
  ngAfterViewInit() {
    $("#logout").css("display","block")
    $(".arrow2").css("display","block");
    $(".arrow1").css("display","block");
  }
  getPosts(): Observable<any[]> {
 


    let httpParams = new HttpParams();
    
    httpParams = httpParams.append('userid', sessionStorage.getItem("userId")?.toString());
    return this.http.get<any[]>('https://digital-h-1-uat.certainteed.com/OktaAPI/App/GetUserApps', { params: httpParams });  
  }

  
}
