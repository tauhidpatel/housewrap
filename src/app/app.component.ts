import { Component, ChangeDetectorRef, OnInit , Inject, LOCALE_ID } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Okta } from './shared/okta/okta.service';
import * as $ from "jquery";
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import {  ElementRef, HostListener, Input } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  mySubscription;
  myDate = new Date();
  title: any;
  oktaSignIn;
  isMenuOpened: boolean = false;
  locale: string;
  flag: string;
  langauge:string;
  
  @HostListener('document:click', ['$event'])
  clickout(event) {
    var target = event.target;
    if(target.parentElement.classList[1]=='utility-drop__trigger') {
      if($(".dropdown--country").hasClass("open"))
      {
        $(".dropdown--country").removeClass("open");
      }
      else
      {
      $(".dropdown--country").addClass("open");
      }
    } else {
      $(".dropdown--country").removeClass("open");
    }
  }
  

  ngOnInit(): void {
this.flag=sessionStorage.getItem('flag');
this.langauge=sessionStorage.getItem('lang');
    if(sessionStorage.getItem('flag'))
   {
    if(sessionStorage.getItem('flag')=='cn')
    {
      $("#flag").removeClass($("#flag")[0].classList[1]);
      $("#flag").addClass('flag-icon-cn');
    //  this.flag="CN"
    
    }
   else if(sessionStorage.getItem('flag')=='us')
    {
      $("#flag").removeClass($("#flag")[0].classList[1]);
      $("#flag").addClass('flag-icon-us');
     // this.flag="USN"
    }
   }
    let cookie_consent = this.getCookie("user_cookie_consent");
    if(cookie_consent != ""){
        document.getElementById("cookieNotice").style.display = "none";
    }else{
        document.getElementById("cookieNotice").style.display = "block";
    }
  //  this.useLanguage(environment.language,this.flag);
  }
  GotoProfile()
  {
    this.router.navigate(['/profile']); 
  }
  GotoHome()
  {
    this.router.navigate(['/dashboard']); 
  }
  constructor(private translate: TranslateService,private router:Router,private okta:Okta,private eRef: ElementRef,@Inject(LOCALE_ID) locale: string) {
  //  translate.setDefaultLang('en');
   // environment.language="en";
   debugger;
   if (sessionStorage.getItem('lang')) {
    console.log('getting old lang');
    console.log(sessionStorage.getItem('lang'));
      this.locale = sessionStorage.getItem('lang');
   }
   else{
    this.locale = navigator.language;

    if(this.locale=='en-US')
    {
      environment.language="en";
      this.flag="us";
      this.langauge=environment.language;
      sessionStorage.setItem('lang', "en");
    sessionStorage.setItem('flag', "us");
    }
    else  if(this.locale=='es-419')
    {
      environment.language="es";
      this.flag="us";
      this.langauge=environment.language;
      sessionStorage.setItem('lang', "es");
      sessionStorage.setItem('flag', "us");
    }
    else  if(this.locale=='es')
    {
      environment.language="es";
      this.flag="us";
      this.langauge=environment.language;
      sessionStorage.setItem('lang', "es");
      sessionStorage.setItem('flag', "us");
    }
    else  if(this.locale=='es-mx')
    {
      environment.language="es";
      this.flag="us";
      this.langauge=environment.language;
      sessionStorage.setItem('lang', "es");
      sessionStorage.setItem('flag', "us");
    }
    else  if(this.locale=='es-us')
    {
      environment.language="es";
      this.flag="us";
      this.langauge=environment.language;
      sessionStorage.setItem('lang', "es");
      sessionStorage.setItem('flag', "us");
    }
    else  if(this.locale=='fr-CA')
    {
    
      environment.language="fr";
      this.flag="cn";
      this.langauge=environment.language;
      sessionStorage.setItem('lang', "fr");
      sessionStorage.setItem('flag', "cn");
     // document.title = "Centre client | CertainTeed";
    }
    else  if(this.locale=='fr-fr')
    {
    
      environment.language="fr";
      this.flag="cn";
      this.langauge=environment.language;
      sessionStorage.setItem('lang', "fr");
      sessionStorage.setItem('flag', "cn");
     // document.title = "Centre client | CertainTeed";
    }
    else  if(this.locale=='en-CA')
    {
      environment.language="en";
      this.flag="cn";
      this.langauge=environment.language;
      sessionStorage.setItem('lang', "en");
      sessionStorage.setItem('flag', "cn");
    }
    else
    {
      environment.language="en";
      this.flag="us";
      this.langauge=environment.language;
      sessionStorage.setItem('lang', "en");
      sessionStorage.setItem('flag', "us");
    }
    alert(this.flag);
  }
    this.translate.use(sessionStorage.getItem('lang'));
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.mySubscription = this.router.events.subscribe((event) => {
        if (event instanceof NavigationEnd) {
    // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
        }
      }); 
  }
  // @HostListener('document:click', ['$event.target'])
  // public onClick(target) {
  //   const clickedInside = this.eRef.nativeElement.contains(target);
  //   if (!clickedInside) {
  //     console.log('outside click xx');
  //     this.eRef.nativeElement.emit();
  //   }
  // }
  logout(): void {
var message="";
    if(sessionStorage.getItem('lang')=="en")
    {
      message="If you logout from here. you are going logged out from all application.Are you sure to logout? ";
    }
    else if(sessionStorage.getItem('lang')=="es")
    {
      message="Si cierra sesión desde aquí. va a cerrar sesión en todas las aplicaciones. ¿Está seguro de cerrar sesión?";
    }
    else
    {
      message="Si vous vous déconnectez d'ici. vous allez vous déconnecter de toutes les applications. Êtes-vous sûr de vous déconnecter ? ";
    }
    if(confirm(message)) {
    this.oktaSignIn = this.okta.getWidget();
    // this.oktaSignIn.authClient.signOut(() => {
    //   this.router.navigate(['/login']); 
      
    // });
    this.oktaSignIn.authClient.signOut({
      clearTokensBeforeRedirect: true,
      postLogoutRedirectUri: 'https://digital-h-1-uat.certainteed.com/customerhub'
    });
  }
  }
  useLanguage(language: string,flag:string) {
    sessionStorage.setItem('lang', language);
    sessionStorage.setItem('flag', flag);
    this.flag=flag;
    this.langauge=language;
    if(flag=='cn')
    {
      $("#flag").removeClass($("#flag")[0].classList[1]);
      $("#flag").addClass('flag-icon-cn');
    }
   else if(flag=='us')
    {
      $("#flag").removeClass($("#flag")[0].classList[1]);
      $("#flag").addClass('flag-icon-us');
    }
  
  
    this.translate.use(language);
    document.querySelector('html')?.setAttribute('lang', language);
    environment.language=language;
       if(language=='fr')
    {
       document.title = "Centre client | CertainTeed";
    }
    window.location.reload();
   // const currentUrl = this.router.url;
   // this.router.navigate([currentUrl])
    debugger;
  }
  
   open(i) {
    $(".utility-drop__trigger")[i].parentElement.classList.toggle("open");
  }

  ngOnDestroy(){
    if (this.mySubscription) {
     this.mySubscription.unsubscribe();
    }


  }
  setCookie(cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    let expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

// Delete cookie
 deleteCookie(cname) {
    const d = new Date();
    d.setTime(d.getTime() + (24*60*60*1000));
    let expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=;" + expires + ";path=/";
}

// Read cookie
 getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

// Set cookie consent
 acceptCookieConsent(){
    this.deleteCookie('user_cookie_consent');
    this.setCookie('user_cookie_consent', 1, 30);
    document.getElementById("cookieNotice").style.display = "none";
}
  openpopup()
  {
    var popup = document.getElementById("cookie_redirect_notice");
    popup.classList.toggle("show");
  }
}
