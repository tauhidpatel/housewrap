import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: any;
  login:string;
  Email:string;
  Firstname:string;
  lastname:string;
  mobile:string;
  Organization:string;
  constructor(private http: HttpClient,private router: Router) { }

  ngOnInit(): void {
if(sessionStorage.getItem("userId")!=null)
{
    this.getuser().subscribe((data: any[]) => {
    //  console.log(data);
    //  this.posts = JSON.parse(data.toString()).filter(a => a.status == "ACTIVE" && a.id != "0oa2rnb6sw4FqFlc34x7" && a.id != "0oa2s3zq6qchwJbUH4x7" && a.id != "0oa5gyxfbxiNYSrcq4x6");
    this.user = JSON.parse(data.toString());
    this.login=this.user?.profile.login;
    this.Firstname=this.user?.profile.firstName;
    this.lastname=this.user?.profile.lastName;
    this.Email=this.user?.profile.email;
    this.Organization=this.user?.profile.organization;
    this.mobile=this.user?.profile.mobilePhone;

     // alert(this.posts);
     // console.log(this.user);
      $("#loader").css("display","none")
    }, error=> console.log(error) );
  }
  else
  {
    this.router.navigate(['/login']); 
  }
  }

  getuser(): Observable<any[]> {
 
    let httpParams = new HttpParams();
    
    httpParams = httpParams.append('userid', sessionStorage.getItem("userId")?.toString());
    return this.http.get<any[]>('https://digital-h-1-uat.certainteed.com/OktaAPI/User/GetUser', { params: httpParams });  
  }

  ChangePassword()
  {
    this.router.navigate(['/ChangePassword']); 
    
  }
  SaveUser()
  {
    debugger;
    let httpParams = new HttpParams();
    
    httpParams = httpParams.append('userid', sessionStorage.getItem("userId")?.toString());
    httpParams = httpParams.append('FirstName', this.Firstname);
    httpParams = httpParams.append('LastName', this.lastname);
    httpParams = httpParams.append('Email', this.Email);
    httpParams = httpParams.append('mobilePhone', this.mobile);
    httpParams = httpParams.append('organization', this.Organization);
    let headers = new HttpHeaders();  
         
  
    headers.append('Accept', 'application/json');
    headers.append("Access-Control-Allow-Origin", "*");
    const myData = { userid:sessionStorage.getItem("userId")?.toString(),FirstName: this.Firstname, LastName: this.lastname,Email: this.Email,organization:this.Organization,mobilePhone:this.mobile};
return this.http.post('https://digital-h-1-uat.certainteed.com/OktaAPI/User/updateuser', myData, { headers });
   // return this.http.post<any[]>('http://localhost:50154/User/updateuser1', { params: httpParams }); 
  }

  save(){
   // console.log(this.login);

    this.SaveUser().subscribe((data: any[]) => {
    //  console.log(data);

      if(data[1]=="OK")
      {
       $("#success").css("display","block");
       $("#Error").css("display","none");
      }
      else
      {
        $("#Error").css("display","block");
        $("#success").css("display","none");
      }
    });
  }

  ngAfterViewInit() {
    $("#logout").css("display","block")
  }
}
