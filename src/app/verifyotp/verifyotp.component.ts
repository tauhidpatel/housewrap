import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgZone } from '@angular/core';

@Component({
  selector: 'app-verifyotp',
  templateUrl: './verifyotp.component.html',
  styleUrls: ['./verifyotp.component.css']
})
export class VerifyotpComponent implements OnInit {

  constructor( 
    private _Activatedroute: ActivatedRoute,
    private http: HttpClient,
    private _router: Router,
    private ngZone: NgZone
    ) { }

  sub;
  userid;
  factorid;

  ngOnInit(): void {
    debugger;
    this.sub = this._Activatedroute.paramMap.subscribe((params) => {
      console.log(params);
      this.userid = params.get('userid');
      this.factorid = params.get('factorid');
      
    });
  }

  async verifyemail() {
   
    var myHeaders1 = new Headers();
    myHeaders1.append("Content-Type", "application/json");
    
    var OTP=$("#password").val();

    // Check if the OTP input is blank
    if (!OTP) {
      // Show the Bootstrap alert for blank OTP
      document.getElementById('blankOTPAlert').style.display = 'block';
  
      // Hide the alert after 3 seconds
      setTimeout(() => {
        document.getElementById('blankOTPAlert').style.display = 'none';
      }, 3000);
  
      return; // Exit the function early if OTP is blank
    }

    let httpParams1 = new HttpParams();
    
    // httpParams1 = httpParams1.append('UserId', userid.toString());
   
    let headers = new HttpHeaders();  
            
     
    headers.append('Accept', 'application/json');
    headers.append("Access-Control-Allow-Origin", "*");
    const myData = {OTP:OTP, UserId:this.userid,factorId:this.factorid};
    console.log(myData);
    this.http.post('http://localhost:50154/App/verifyotp', myData, { headers }).subscribe((data: any[]) => {
       debugger;
    // data=  JSON.parse(data[0].toString());  
    // console.log(data)
    console.log(data);
    //console.log(data.errorSummary);
    //console.log(data[1]) // OK
    // console.log(data[1].factorResult);
    
    if (data[1] === "OK") { 
      // Show the success alert and hide the danger alert
      this.ngZone.run(() => {
        document.getElementById('otpSuccessAlert').style.display = 'block';
      });
    
      // Hide the alert after 2 seconds
      setTimeout(() => {
        this.ngZone.run(() => {
          document.getElementById('otpSuccessAlert').style.display = 'none';
        });
      }, 2000);
      
      document.getElementById('otpDangerAlert').style.display = 'none';
    } else if (data[1] === "Forbidden") {
      // Show the danger alert and hide the success alert
      document.getElementById('otpSuccessAlert').style.display = 'none';
      this.ngZone.run(() => {
        document.getElementById('otpDangerAlert').style.display = 'block';
      });

      setTimeout(() => {
        this.ngZone.run(() => {
          document.getElementById('otpDangerAlert').style.display = 'none';
        });
      }, 2000)
    }
   });
  
  }
}
