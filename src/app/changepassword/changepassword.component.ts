import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {

  constructor() { }
  ngAfterViewInit() {
    $("#logout").css("display","block")
  }
  ngOnInit(): void {
  }
  async createuser(){
   
    var myHeaders1 = new Headers();
    myHeaders1.append("Content-Type", "application/json");
    
     var password=$("#password").val();
     
       var confirm_password=$("#confirmPassword").val();
       if(password!="" || confirm_password!="")
    {
      
       $("#cover-spin").show();
       var settings = {
        "url": "https://digital-h-1-uat.certainteed.com/OktaAPI/app/changepassword?oldPassword="+password+"&newPassword="+confirm_password+"&UserId="+sessionStorage.getItem("userId")?.toString()+"",
        "method": "POST",
        "timeout": 0,
      };
      
      $.ajax(settings).done(function (response) {
        debugger;
        console.log(response);
        var res=response[1];
        if(res="Forbidden")
        {
          $("#cover-spin").hide();
          $("#alert-danger").css("display", "block");
        }
        else
        {
         
          $("#cover-spin").hide();
 $(".alert-success").css("display", "block");
        }
      });
    
//        $.ajax({
//         type: "POST",
//         url: "http://localhost:50154/app/changepassword",
//         // data: {"User": sessionStorage.getItem("userId")?.toString() ,"Password": confirm_password},
//         data: {"oldPassword": $("#password").val().toString() ,"newPassword": $("#confirmPassword").val().toString(),"UserId":"00u5nm5w9ci0roECa0x7"},
//         contentType: "application/json; charset=utf-8",
//         dataType: "json",
//         success: function (response) {
//           debugger;
// if(response[1]=="BadRequest")
// {
// $("#cover-spin").hide();
// $(".wrong").css("display", "block");
// }
// else{
// var a= response;
// $("#cover-spin").hide();
// $(".alert-success").css("display", "block");
// // this.router.navigate(['/dashboard']); 
// }
// },
// error: function (response) {
//   console.log(response);
// debugger;
// $("#wrong").css("display", "none");
// $("#Enter").css("display", "none");
// $("#alert-danger").css("display", "block");
// }

// });
   
    }
    else
      {
      $(".Enter").css("display", "block");
      }

    
    
  }
}
    

