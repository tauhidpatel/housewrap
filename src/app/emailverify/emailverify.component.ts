import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { NgZone } from '@angular/core';

@Component({
  selector: 'app-emailverify',
  templateUrl: './emailverify.component.html',
  styleUrls: ['./emailverify.component.css']
})
export class EmailverifyComponent implements OnInit {

  constructor(private http: HttpClient,private router:Router, private ngZone: NgZone) { }

  ngOnInit(): void { }

  async verifyemail(){
   
    var myHeaders1 = new Headers();
    myHeaders1.append("Content-Type", "application/json");
    
    var Email=$("#password").val();

    // Check if the email input is blank
    if (!Email) {
      document.getElementById('blankEmailAlert').style.display = 'block';

      setTimeout(() => {
        document.getElementById('blankEmailAlert').style.display = 'none';
      }, 3000);

      return; // Exit the function early if email is blank
    }

    // Clear any previous alerts
    document.getElementById('blankEmailAlert').style.display = 'none';

    let httpParams = new HttpParams();
    var userid;
    httpParams = httpParams.append('Email', Email.toString());
    this.http.get<any[]>('http://localhost:50154/User/GetUserUserid', { params: httpParams }).subscribe((data: any[]) => {
      debugger;
    data=  JSON.parse(data.toString());   
     userid= data[0].id;
      console.log(data[0].status);

      if (data[0].status === "ACTIVE") {
        // Display the Bootstrap alert for 2 seconds
        this.ngZone.run(() => {
          document.getElementById('userStatusAlert').style.display = 'block';
        });
      
        // Hide the alert after 2 seconds
        setTimeout(() => {
          this.ngZone.run(() => {
            document.getElementById('userStatusAlert').style.display = 'none';
          });
        }, 2000);
      } else if( data[0].status=="PROVISIONED" ) {
        let httpParams1 = new HttpParams();
        httpParams1 = httpParams.append('UserId', userid.toString());
        let headers = new HttpHeaders();  
        headers.append('Accept', 'application/json');
        headers.append("Access-Control-Allow-Origin", "*");
        const myData = { UserId:userid.toString() };
  
        this.http.post('http://localhost:50154/App/EmailChallange', myData, { headers }).subscribe((data: any[]) => {
        debugger;
        // data=  JSON.parse(data.toString());  
        //var userid= data[0].id;
        console.log(data[0]);
        document.getElementById('userStatusSuccess').style.display = 'block';
        // alert('Email verified');
        // this.router.navigate(['/verifyotp',userid,data[0]]); 
        setTimeout(() => {
          this.router.navigate(['/verifyotp', userid, data[0]]);
        }, 3000);
      });
      }
  });
  
  }
}
