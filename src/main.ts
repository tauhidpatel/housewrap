import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  if(!sessionStorage.getItem('lang'))
  {
  
    //sessionStorage.setItem('lang', navigator.language);
    if(navigator.language=='en-US')
    {
      sessionStorage.setItem('lang', "en");
    sessionStorage.setItem('flag', "us");
    }
    else  if(navigator.language=='es-419')
    {
     
      sessionStorage.setItem('lang', "es");
      sessionStorage.setItem('flag', "us");
    }
    else  if(navigator.language=='es')
    {
      
      sessionStorage.setItem('lang', "es");
      sessionStorage.setItem('flag', "us");
    }
    else  if(navigator.language=='es-mx')
    {
    
      sessionStorage.setItem('lang', "es");
      sessionStorage.setItem('flag', "us");
    }
    else  if(navigator.language=='es-US')
    {
      
      sessionStorage.setItem('lang', "es");
      sessionStorage.setItem('flag', "us");
    }
    else  if(navigator.language=='fr-CA')
    {
    
      
      sessionStorage.setItem('lang', "fr");
      sessionStorage.setItem('flag', "cn");
     // document.title = "Centre client | CertainTeed";
    }
    else  if(navigator.language=='fr-fr')
    {
     
      sessionStorage.setItem('lang', "fr");
      sessionStorage.setItem('flag', "cn");
     // document.title = "Centre client | CertainTeed";
    }
    else  if(navigator.language=='en-CA')
    {
      
      sessionStorage.setItem('lang', "en");
      sessionStorage.setItem('flag', "cn");
    }
    else
    {
      
      sessionStorage.setItem('lang', "en");
      sessionStorage.setItem('flag', "us");
    }
   // sessionStorage.setItem('flag', "us");
  }
 
  document.querySelector('html')?.setAttribute('lang',sessionStorage.getItem('lang'));
  
  if(sessionStorage.getItem('lang')=='en')
      {
        document.title = "Customer Hub | CertainTeed";
      }
      else  if(sessionStorage.getItem('lang')=='es')
      {
        document.title = "Centro de clientes | CertainTeed";
      }
      
      else  if(sessionStorage.getItem('lang')=='fr')
      {
        document.title = "Centre client | CertainTeed";
      }
      else
      {
        document.title = "Customer Hub | CertainTeed";
      }
  const script = document.createElement("script");
  script.setAttribute("data-account", "klq7KbDiT8");
  script.setAttribute("src", "https://cdn.userway.org/widget.js");
  document.head.appendChild(script);
  enableProdMode();
    
}
else
{
  debugger;
  if(!sessionStorage.getItem('lang'))
  {
   
    //sessionStorage.setItem('lang', navigator.language);
    if(navigator.language=='en-US')
    {
      sessionStorage.setItem('lang', "en");
    sessionStorage.setItem('flag', "us");
    }
    else  if(navigator.language=='es-419')
    {
     
      sessionStorage.setItem('lang', "es");
      sessionStorage.setItem('flag', "us");
    }
    else  if(navigator.language=='es')
    {
      
      sessionStorage.setItem('lang', "es");
      sessionStorage.setItem('flag', "us");
    }
    else  if(navigator.language=='es-mx')
    {
    
      sessionStorage.setItem('lang', "es");
      sessionStorage.setItem('flag', "us");
    }
    else  if(navigator.language=='es-US')
    {
      
      sessionStorage.setItem('lang', "es");
      sessionStorage.setItem('flag', "us");
    }
    else  if(navigator.language=='fr-CA')
    {
    
      
      sessionStorage.setItem('lang', "fr");
      sessionStorage.setItem('flag', "cn");
     // document.title = "Centre client | CertainTeed";
    }
    else  if(navigator.language=='fr-fr')
    {
     
      sessionStorage.setItem('lang', "fr");
      sessionStorage.setItem('flag', "cn");
     // document.title = "Centre client | CertainTeed";
    }
    else  if(navigator.language=='en-CA')
    {
      
      sessionStorage.setItem('lang', "en");
      sessionStorage.setItem('flag', "cn");
    }
    else
    {
      
      sessionStorage.setItem('lang', "en");
      sessionStorage.setItem('flag', "us");
    }
   // sessionStorage.setItem('flag', "us");
  }
  document.querySelector('html')?.setAttribute('lang',sessionStorage.getItem('lang'));
  if(sessionStorage.getItem('lang')=='en')
      {
        document.title = "Customer Hub | CertainTeed";
      }
      else  if(sessionStorage.getItem('lang')=='es')
      {
        document.title = "Centro de clientes | CertainTeed";
      }
      
      else  if(sessionStorage.getItem('lang')=='fr')
      {
        document.title = "Centre client | CertainTeed";
      }
      else
      {
        document.title = "Customer Hub | CertainTeed";
      }
  const script = document.createElement("script");
  script.setAttribute("data-account", "klq7KbDiT8");
  script.setAttribute("src", "https://cdn.userway.org/widget.js");
  document.head.appendChild(script);
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
